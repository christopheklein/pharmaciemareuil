var libraries = [];
libraries.push('jquery');
libraries.push('flexnav');
libraries.push('sticky');
libraries.push('cycle2');

define(libraries, function ($) {

	$(document).ready(function() {

        // Sous-menus
        $('.flexnav').flexNav({
            animationSpeed: 0
        });
        $("#primary").sticky({
            topSpacing: 0
        });
        $(".menu-button").sticky({
            topSpacing: 0
        });

		$( '.slideshow' ).cycle({
			timeout: 4500
					});

	});

    (function($){
        // REPLACE ALL SVG UIMAGES WITH INLINE SVG AT 1ST DOCUMENT LOAD
        $(document).ready(function(){
            $('img.svg').each(function(){
                replaceImgWithSVG($(this));
            });
        });

        // REPLACE ALL SVG IMAGE WITH INLINE SVG AFTER AJAX LOAD
        $(document).ajaxComplete(function(e, jqXHR, ajaxOptions) {
            if(ajaxOptions.isSvgCustomLoad == true) {
                // console.log('svg loaded');
                return;
            }
            $('img.svg').each(function(){
                replaceImgWithSVG($(this));
            });
        });

    })(jQuery);

    function replaceImgWithSVG($img){
        var imgID   = $img.attr('id');
        var imgClass    = $img.attr('class');
        var imgURL      = $img.attr('src');
        var imgWidth    = $img.attr('width');
        var imgHeight   = $img.attr('height');

        if(typeof imgURL == 'string') {
            $.ajax({
                url: imgURL,
                success: function(data) {
                    // Get the SVG tag, ignore the rest
                    var $svg = $(data).find('svg');

                    // Add replaced image's ID to the new SVG
                    if(typeof imgID !== 'undefined') {
                        $svg = $svg.attr('id', imgID);
                    }
                    // Add replaced image's classes to the new SVG
                    if(typeof imgClass !== 'undefined') {
                        $svg = $svg.attr('class', imgClass+' replaced-svg');
                    }

                    if(typeof imgWidth !== 'undefined'){
                        $svg = $svg.attr('width', imgWidth);
                    }

                    if(typeof imgHeight !== 'undefined'){
                        $svg = $svg.attr('height', imgHeight);
                    }

                    // Remove any invalid XML tags as per http://validator.w3.org
                    $svg = $svg.removeAttr('xmlns:a');

                    // Replace image with new SVG
                    $img.replaceWith($svg);
                },
                dataType: 'xml',
                isSvgCustomLoad: true
            });
        }
    }

});
