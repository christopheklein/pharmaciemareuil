var _paths = {};
_paths.requirejs = "../vendor/requirejs";
_paths.jquery = "../vendor/jquery/dist/jquery.min";
_paths.flexnav = "../vendor/flexnav/js/jquery.flexnav.min";
_paths.sticky = "../vendor/jquery-sticky/jquery.sticky";
_paths.cycle2 = "../vendor/jquery-cycle2/build/jquery.cycle2.min";

var _shim = {};
_shim.flexnav = ['jquery'];
_shim.sticky = ['jquery'];
_shim.cycle2 = ['jquery'];

requirejs.config({
	baseUrl: 'assets/js',
    paths: _paths,
    shim: _shim
});

require(['app/homepage']);
