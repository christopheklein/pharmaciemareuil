<div id="news_coords">
	<div id="news">
		<h2>News</h2>
		<p>Utque proeliorum periti rectores primo catervas densas opponunt et fortes, deinde leves armaturas, post iaculatores ultimasque subsidiales acies, si fors adegerit, iuvaturas, ita praepositis urbanae familiae suspensae digerentibus sollicite, quos insignes faciunt virgae dexteris aptatae velut tessera data castrensi iuxta vehiculi fronte</p>
	</div>
	<div id="coords">
		<div><img class="svg" src="assets/img/pin.svg" alt=""></div>
		<div id="tel">01 64 33 69 99</div>
		<div id="adresse">Pharmacie principale de Mareuil<br>
Centre Commercial E. Leclerc - 30 Mail de la Grande Haie<br>
77100 Mareuil lès Meaux</div>
		<div id="horaires">
			Lundi au vendredi : De 9h00 à 20h00
			<strong>Sans interruption</strong>
		</div>
	</div>
</div>