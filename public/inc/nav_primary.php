<nav id="primary" role="primary">
	<ul class="flexnav" data-breakpoint="1024">
		<li><a href="index.php">Accueil</a></li>
		<li><a href="page.php">Officine</a>
			<ul>
				<li><a href="page.php">Equipe</a></li>
				<li><a href="page.php">Horaires</a></li>
				<li><a href="page.php">Plan</a></li>
			</ul>
		</li>
		<li><a href="page.php">Services</a>
			<ul>
				<li><a href="page.php">Ordonnance en ligne</a></li>
				<li><a href="page.php">Livraison &agrave; domicile</a></li>
				<li><a href="page.php">Pharmacovigilance</a></li>
				<li><a href="page.php">Pharmacie de garde</a></li>
				<li><a href="page.php">Num&eacute;ros utiles</a></li>
				<li><a href="page.php">Liens utiles</a></li>
			</ul>
		</li>
		<li><a href="page.php">Sp&eacute;cialit&eacute;s</a>
			<ul>
				<li><a href="page.php">Othop&eacute;die</a></li>
				<li><a href="page.php">Mat&eacute;riel m&eacute;dical</a></li>
				<li><a href="page.php">M&eacute;decine douce</a></li>
				<li><a href="page.php">Nutrition</a></li>
				<li><a href="page.php">Cosm&eacute;tique</a></li>
				<li><a href="page.php">Hygi&egrave;ne</a></li>
				<li><a href="page.php">Produits capillaires</a></li>
				<li><a href="page.php">Pu&eacute;riculture</a></li>
				<li><a href="page.php">Marques</a></li>
			</ul>
		</li>
		<li><a href="page.php">News</a>
			<ul>
				<li><a href="page.php">Actualit&eacute;s</a></li>
				<li><a href="page.php">Promotions</a></li>
				<li><a href="page.php">Nouveaut&eacute;s</a></li>
			</ul>
		</li>
		<li><a href="page.php">Contact</a></li>
	</ul>
	<div class="menu-button">Menu</div>
</nav>
