<section id="slider_main">
	<div class="slideshow">
		<img src="assets/img/slider/main/1.jpg">
		<img src="assets/img/slider/main/2.jpg">
	</div>
	<div id="slider_main_overlay">
		<h2>La Pharmacie Principale de Mareuil</h2>
		<p>vous accueille dans le centre commercial E Leclerc de Mareuil les Meaux, en Seine-et-Marne (77).</p>
		<p> Avec dynamisme et s&eacute;rieux, l&rsquo;&eacute;quipe officinale de la Pharmacie Principale de Mareuil vous propose une offre globale du mieux-vivre r&eacute;pondant &agrave; vos l&rsquo;ensemble de vos besoins en mati&egrave;re de sant&eacute; et de bien-&ecirc;tre.</p>
	</div>
</section>
